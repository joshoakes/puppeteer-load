const { Cluster } = require('puppeteer-cluster');
const { PerfCounters, CounterSummary } = require('./perfCounters');
const { testMethods } = require('./testCases');

async function main() {
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 10,
        timeout: 120000,
        monitor: true
      });

      console.log("== Test run started with 10 concurrent browsers");

      var perfCounters = new PerfCounters();

      await cluster.task(async ({ page, data: url }) => {        
        let counters = await testMethods.testUrlLoadTime(page, url, 20);
        perfCounters.addCounters(counters);
      });
    
      // many more pages
      cluster.queue('http://public.localmachine.altsrc.net/');
      cluster.queue('http://public.localmachine.altsrc.net/Shopping/choose/plan');
      cluster.queue('http://public.localmachine.altsrc.net/shopping/choose/device');
      cluster.queue('http://public.localmachine.altsrc.net/WhyUs');
      cluster.queue('http://public.localmachine.altsrc.net/Help/videos-and-manuals');
      cluster.queue('http://public.localmachine.altsrc.net/Shopping/choose/plan');
      cluster.queue('http://public.localmachine.altsrc.net/shopping/choose/device');
      cluster.queue('http://public.localmachine.altsrc.net/Shopping/choose/plan');
      cluster.queue('http://public.localmachine.altsrc.net/shopping/choose/device');
      cluster.queue('http://public.localmachine.altsrc.net/Help/videos-and-manuals');
    
      await cluster.idle();
      await cluster.close();

      console.log("== Test run complete");

      let counterSummaries = perfCounters.summarizeCounters();
      prettyPrintSummaries(counterSummaries);
}

function prettyPrintSummaries(counterSummaries) {

    const urlLength = 70;
    const numberLength = 12;

    const divider = "-".repeat(urlLength + numberLength * 6);

    console.log(divider);

    let headers = [];
    headers.push("URL".padEnd(urlLength));
    headers.push("# SAMPLES".padEnd(numberLength));
    headers.push("# ERRORS".padEnd(numberLength));
    headers.push("% ERROR".padEnd(numberLength));
    headers.push("MIN(ms)".padEnd(numberLength));
    headers.push("MAX(ms)".padEnd(numberLength));
    headers.push("AVG(ms)".padEnd(numberLength));

    console.log(headers.join(""));

    console.log(divider);

    counterSummaries.forEach(summary => {
      let pretty = [];

      pretty.push(summary.url.padEnd(urlLength));
      pretty.push(summary.samples.toString().padEnd(numberLength));
      pretty.push(summary.errors.toString().padEnd(numberLength));
      pretty.push(summary.errorPct.toFixed(2).toString().padEnd(numberLength));
      pretty.push(Math.trunc(summary.min).toString().padEnd(numberLength));
      pretty.push(Math.trunc(summary.max).toString().padEnd(numberLength));
      pretty.push(Math.trunc(summary.avg).toString().padEnd(numberLength));

      console.log(pretty.join(""));
    });

    console.log();
}

main()
  // .then(console.log)
  .catch(console.error)