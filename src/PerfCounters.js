class PerfCounters {
    constructor() {
        this.counters = {};
    }

    addCounter(counter) {
        if (counter.url in this.counters) {
            this.counters[counter.url].push(counter);
        }
        else {
            this.counters[counter.url] = [counter];
        }
    }

    addCounters(counters) {
        counters.forEach(c => this.addCounter(c));
    }

    summarizeCounters() {
        var summaries = [];
        Object.entries(this.counters).forEach(function([key, value]) {
            
            let samples = value.length;
            let timesTaken = value.filter(c => c.success).map(c => c.timeTakenMs);
            let errors = value.map(c => c.success).filter(c => !c);
            let summary = new CounterSummary(
                key, 
                samples,
                Math.min(...timesTaken),
                Math.max(...timesTaken),
                Math.sum(timesTaken) / timesTaken.length,
                errors.length,
                (errors.length / samples) * 100);
            
            summaries.push(summary);
        });

        return summaries;
    }
}

class Counter {
    constructor(url, timeTakenMs, success, error) {
        this.url = url;
        this.timeTakenMs = timeTakenMs;
        this.success = success;
        this.error = error;
    }
}

class CounterSummary {
    
    constructor(url, samples, min, max, avg, errors, errorPct) {
        this.url = url;
        this.samples = samples;
        this.min = min;
        this.max = max;
        this.avg = avg;
        this.errors = errors;
        this.errorPct = errorPct;
    }
}

Math.sum = function() {
    var values = arguments.length && arguments.length == 1 ? arguments[0] : [];
    if (Array.isArray(values) == false) {
        return 0;
    }
    return values.reduce((a, b) => a + b, 0);
};

exports.PerfCounters = PerfCounters;
exports.Counter = Counter;
exports.CounterSummary = CounterSummary;

