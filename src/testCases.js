const { Counter } = require('./perfCounters');

async function testUrlLoadTime(page, url, iterations) {

    var counters = [];

    for (var i = 0; i < iterations; i++) {

        try {
            let before = Date.now();
            let response = await page.goto(url, { "waitUntil": "networkidle0", timeout: 240000 });
            let taken = Date.now() - before - 500; // substract 500 to account for networkidle0
            if (response && (response.ok() || response.status() == 304)) {
                counters.push(new Counter(url, taken, true));
            }
            else {
                counters.push(new Counter(url, taken, false));
            }        
        }        
        catch (e) {
            counters.push(new Counter(url, -1, false, e));
        }
    }

    return counters;
}

exports.testMethods = { testUrlLoadTime };